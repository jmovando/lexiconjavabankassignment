Prerequisites:
--------------
Knowledge of Java5 (or above), Git, BitBucket, (SourceTree), and Maven (or be a quick learner).

Java version
----------------------------------------
The source code requires Java5, however you are free implement your solution with a newer version. 

Suggested tools to install
--------------------------------------
Apache Maven
SourceTree Git client
Your IDE of choice
Java 5 or above

Test guidlines
-------------------------------------- 

1. Fork this repository.
2. Clone it localy.
3. Commit and push to your forked repository as many times as you want during work.
4. When done, send link to your forked repository to the test leader that will have a look at your solution.


Test assignment
----------------------------------------------

The source code is a Java project set up as a Maven project. 
The test cases are failing. 
Make the test cases pass by editing the files in the impl package.

Your implementation must obviously be thread safe. 
Finalise the method in AccountImplConcurrencyTest so that it prove that 
the AccountState is never corrupt (with resonable probability).
Use AccountStateUtil to verify the account state.    

It is allowed to create new files in the impl package (both under src/main/java and src/test/java).  
"Sub packages" to the impl package are also allowed. 


Usefull links
--------------
https://confluence.atlassian.com/display/BITBUCKET/Bitbucket+101
http://maven.apache.org/guides/getting-started/maven-in-five-minutes.html