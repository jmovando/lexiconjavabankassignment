package se.lexicion.javabankassignment.impl;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.HashSet;

import org.junit.Test;

import se.lexicon.javabankassignment.Transaction;
import se.lexicon.javabankassignment.TransactionFactory;
import se.lexicon.javabankassignment.impl.TransactionFactoryImpl;


public class TransactionTest {

	TransactionFactory transactionFactory = new TransactionFactoryImpl();
	
	@Test
	public void testTransactionsInHashSet(){
		HashSet<Transaction> hashSet = new HashSet<Transaction>();
		Transaction transaction1a = transactionFactory.createTransaction("transaction1", new BigDecimal(10));
		Transaction transaction1b = transactionFactory.createTransaction("transaction1", new BigDecimal(20));//same Id, different amount
		hashSet.add(transaction1a);
		hashSet.add(transaction1b);
		assertEquals(1, hashSet.size());
	}
	
	@Test
	public void testPrint(){
		Transaction transaction1 = transactionFactory.createTransaction("transaction1", new BigDecimal(10));
		assertEquals("transaction1", ""+transaction1);
	}
	
	
}
