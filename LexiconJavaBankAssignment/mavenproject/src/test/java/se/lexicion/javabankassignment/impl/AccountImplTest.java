package se.lexicion.javabankassignment.impl;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Iterator;

import org.junit.Before;
import org.junit.Test;

import se.lexicon.javabankassignment.Transaction;
import se.lexicon.javabankassignment.TransactionFactory;
import se.lexicon.javabankassignment.impl.AccountImpl;
import se.lexicon.javabankassignment.impl.TransactionFactoryImpl;

public class AccountImplTest {

	AccountImpl accountImpl;

	TransactionFactory transactionFactory = new TransactionFactoryImpl();

	@Before
	public void setUp() {
		accountImpl = new AccountImpl();
	}

	@Test
	public void testDepositAndWithdraw() {
		Transaction transaction1 = transactionFactory.createTransaction("t1",
				new BigDecimal(10));
		accountImpl.makeTransaction(transaction1);
		assertEquals(transaction1, accountImpl.getTransaction("t1"));
		assertEquals(new BigDecimal(10), accountImpl.getTransaction("t1")
				.getAmount());

		Transaction transaction2 = transactionFactory.createTransaction("t2",
				new BigDecimal(-1.5));
		accountImpl.makeTransaction(transaction2);
		assertEquals(transaction2, accountImpl.getTransaction("t2"));
		assertEquals(new BigDecimal(-1.5), accountImpl.getTransaction("t2")
				.getAmount());

		assertEquals(new BigDecimal(8.5), accountImpl.getBalance());

		assertEquals(2, accountImpl
				.getAllTransactionsOrderedByTransactionTime().size());
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testHackerDeletesTransaction() {
		Transaction validTransaction = transactionFactory.createTransaction(
				"t1", new BigDecimal(10));
		accountImpl.makeTransaction(validTransaction);
		try {
			accountImpl.getAllTransactionsOrderedByTransactionTime().remove(
					validTransaction);
		} finally {
			assertEquals(1, accountImpl
					.getAllTransactionsOrderedByTransactionTime().size());
			assertEquals(new BigDecimal(10), accountImpl.getBalance());
		}

	}

	@Test(expected = UnsupportedOperationException.class)
	public void testHackerAddsTransaction() {
		Transaction invalidTransaction = transactionFactory.createTransaction(
				"t1", new BigDecimal(10));
		try {
			accountImpl.getAllTransactionsOrderedByTransactionTime().add(
					invalidTransaction);
		} finally {
			assertEquals(0, accountImpl
					.getAllTransactionsOrderedByTransactionTime().size());
			assertEquals(new BigDecimal(0), accountImpl.getBalance());
		}
	}

	@Test(expected = UnsupportedOperationException.class)
	public void testHackerDeletesTransactionFromAccountState() {
		Transaction validTransaction = transactionFactory.createTransaction(
				"t1", new BigDecimal(10));
		accountImpl.makeTransaction(validTransaction);
		try {
			accountImpl.getAccountState().getAllTransactions()
					.remove(validTransaction);
		} finally {
			assertEquals(1, accountImpl
					.getAllTransactionsOrderedByTransactionTime().size());
			assertEquals(new BigDecimal(10), accountImpl.getBalance());
		}

	}

	@Test(expected = UnsupportedOperationException.class)
	public void testHackerAddsTransactionToAccountState() {
		Transaction invalidTransaction = transactionFactory.createTransaction(
				"t1", new BigDecimal(10));
		try {
			accountImpl.getAccountState().getAllTransactions()
					.add(invalidTransaction);
		} finally {
			assertEquals(0, accountImpl
					.getAllTransactionsOrderedByTransactionTime().size());
			assertEquals(new BigDecimal(0), accountImpl.getBalance());
		}
	}

	@Test
	public void testSortOrder() {
		long timestamp1 = new Date().getTime();
		long timestamp2 = timestamp1 + 1000;
		long timestamp3 = timestamp2 + 1000;
		long timestamp4 = timestamp3 + 1000;
		long timestamp5 = timestamp4 + 1000;
		BigDecimal amount = new BigDecimal(10);
		Transaction transaction3 = transactionFactory.createTransaction("t2",
				amount, timestamp3);
		accountImpl.makeTransaction(transaction3);
		Transaction transaction5 = transactionFactory.createTransaction("t1",
				amount, timestamp5);
		accountImpl.makeTransaction(transaction5);
		Transaction transaction1 = transactionFactory.createTransaction("t5",
				amount, timestamp1);
		accountImpl.makeTransaction(transaction1);
		Transaction transaction4 = transactionFactory.createTransaction("t4",
				amount, timestamp4);
		accountImpl.makeTransaction(transaction4);
		Transaction transaction2 = transactionFactory.createTransaction("t3",
				amount, timestamp2);
		accountImpl.makeTransaction(transaction2);

		Iterator<Transaction> byTransactionId = accountImpl
				.getAllTransactionsOrderedByTransactionId().iterator();
		assertEquals("t1", byTransactionId.next().getTransactionId());
		assertEquals("t2", byTransactionId.next().getTransactionId());
		assertEquals("t3", byTransactionId.next().getTransactionId());
		assertEquals("t4", byTransactionId.next().getTransactionId());
		assertEquals("t5", byTransactionId.next().getTransactionId());

		Iterator<Transaction> byTime = accountImpl
				.getAllTransactionsOrderedByTransactionTime().iterator();
		assertEquals(transaction1, byTime.next());
		assertEquals(transaction2, byTime.next());
		assertEquals(transaction3, byTime.next());
		assertEquals(transaction4, byTime.next());
		assertEquals(transaction5, byTime.next());
	}

}
