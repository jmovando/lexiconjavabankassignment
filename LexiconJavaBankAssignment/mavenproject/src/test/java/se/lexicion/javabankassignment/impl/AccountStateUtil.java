package se.lexicion.javabankassignment.impl;

import java.math.BigDecimal;
import java.util.Collection;

import se.lexicon.javabankassignment.AccountState;
import se.lexicon.javabankassignment.Transaction;

public class AccountStateUtil {
	
	public static boolean accountStateCorrupt(AccountState accountState){
		BigDecimal transactionsSum = sumTransactionAmounts(accountState.getAllTransactions());
		return ! accountState.getBalance().equals(transactionsSum);
	}
	
	public static BigDecimal sumTransactionAmounts(Collection<Transaction> transactions) {
		BigDecimal ret = new BigDecimal(0);
		for (Transaction transaction : transactions) {
			ret = ret.add(transaction.getAmount());
		}
		return ret;
	}

}
