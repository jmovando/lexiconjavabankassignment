package se.lexicon.javabankassignment;

import java.math.BigDecimal;

public interface TransactionFactory {
	
	/**
	 * Use current time as transactionTime. 
	 */
	Transaction createTransaction(String transactionId, BigDecimal amount);

	Transaction createTransaction(String transactionId, BigDecimal amount, long transactionTimeStamp);

}
