package se.lexicon.javabankassignment;

import java.math.BigDecimal;
import java.util.Collection;

public interface AccountState {
	
	Collection<Transaction> getAllTransactions();
	
	BigDecimal getBalance();

}
