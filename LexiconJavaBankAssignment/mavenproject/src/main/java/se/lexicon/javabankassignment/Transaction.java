package se.lexicon.javabankassignment;

import java.math.BigDecimal;


public interface Transaction {
	
	public String getTransactionId();

	public BigDecimal getAmount();

	public long getTransactionTimeStamp();
	
}
