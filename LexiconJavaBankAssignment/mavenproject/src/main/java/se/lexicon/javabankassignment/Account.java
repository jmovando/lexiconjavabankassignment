package se.lexicon.javabankassignment;

import java.math.BigDecimal;
import java.util.List;

public interface Account {
	
	/**
	 * @return the account balance (swedish:kontots saldo) after the transaction has been performed
	 */
	public BigDecimal makeTransaction(Transaction transaction);
	
	public Transaction getTransaction(String transactionId);
	
	public List<Transaction> getAllTransactionsOrderedByTransactionTime();
	
	public List<Transaction> getAllTransactionsOrderedByTransactionId();
	
	public BigDecimal getBalance();
	
	public AccountState getAccountState();

}
